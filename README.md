# About #
These scripts were created to simplify the creation of FTP account on www01-auultimo (ftp12) and the mounting of shared drive located in p-nas22-gsw.

# Usage #

```
#!bash
Usage: ./create_ftp_user.pl -u <username> -p [ftp|sftp] -j <jira> -r <remote directory>
        -u      : Username
        -p      : File transfer protocol to be used
        -j      : JIRA ticket number
        -r      : Remote directory that will be used as mount point

Example:
        ./create_ftp_user.pl -u p_slm_12345_ftp -p ftp -j sdsup-54321 -r 10.20.0.14:/export/hanfs/wip/12345
```

```
#!bash
Usage: ./nas_dir_acc.pl -u <uid> -p [ftp|sftp] -d <directory>
	-u      : UID
	-p      : File transfer protocol to be used
	-d      : Local directory that will be used as mount point for www01-auultimo

Example:
	./nas_dir_acc.pl -u 5166 -p ftp -d /export/hanfs/wip/12345
```
