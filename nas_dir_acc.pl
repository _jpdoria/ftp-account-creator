#!/usr/bin/perl
# Script: nas_dir_acc.pl
# Author: JP Doria <iam@jpdoria.com>
# Note:  2014-08-15 | Initial script for creating directories and granting of privileges for FTP users in p-nas22-gsw.

use strict;
use warnings;
use Getopt::Std;
use File::Path;

# Check root
my $login = getpwuid $>;
if ($login ne 'root') {
        die "Error: must run as root\n";
}

# Display instructions
sub usage {
	die << "EOF";
Usage: $0 -u <uid> -p [ftp|sftp] -d <directory>
	-u      : UID
	-p      : File transfer protocol to be used
	-d      : Local directory that will be used as mount point for www01-auultimo
	
Example:
	$0 -u 5166 -p ftp -d /export/hanfs/wip/12345
EOF
}

# Get options
my %opt;
my $opt_strings = 'u:p:d:';
getopts($opt_strings, \%opt);

# Pre-flight checks
usage if !$opt{u} || !$opt{p} || !$opt{d};

# Check/set params
if (lc($opt{p}) eq 'ftp') {
	$opt{p} = 104;
} elsif (lc($opt{p}) eq 'sftp') {
	$opt{p} = 2008;
}

# Create directories
my $perm = 0755;
my $upload = "$opt{d}/upload";
my $ssh = "$opt{d}/.ssh";
my @dirs = ("$opt{d}", "$upload", "$ssh");
for my $dir (@dirs) {
	if (!-e $dir) {
		mkpath $dir, {
			verbose => 1,
			mode => $perm
		};
		chown 0, 0, $opt{d};
		chown $opt{u}, $opt{p}, $upload;
		chown $opt{u}, $opt{p}, $ssh;
		chmod 0700, $ssh;
	} else {
		print "Error: $dir already exists\n";
		next;
	}
}

