#!/usr/bin/perl
# Script: create_ftp_user.pl
# Author: JP Doria <iam@jpdoria.com>
# Note: 2014-08-15 | Initial script for creating a user account and updating of /etc/vfstab on www01-auultimo.

use strict;
use warnings;
use POSIX 'strftime';
use Getopt::Std;
use File::Copy;
use Digest::MD5;

# Check root
my $login = getpwuid $>;
if ($login ne 'root') {
        die "Error: must run as root\n";
}

# Display instructions
sub usage {
        die << "EOF";
Usage: $0 -u <username> -p [ftp|sftp] -j <jira> -r <remote directory>
        -u      : Username
        -p      : File transfer protocol to be used
        -j      : JIRA ticket number
        -r      : Remote directory that will be used as mount point

Example:
        $0 -u p_slm_12345_ftp -p ftp -j sdsup-54321 -r 10.20.0.14:/export/hanfs/wip/12345
EOF
}

# Get options
my %opt;
my $opt_strings = 'u:p:j:r:';
getopts($opt_strings, \%opt);

# Pre-flight checks
usage if !$opt{u} || !$opt{p} || !$opt{j} || !$opt{r};

# Read $password_file
my $password_file = '/etc/passwd';
open (PWD, "<$password_file") || die "Error: $!\n";
        my @lines = (<PWD>);
close PWD;

# Get the latest UID
my @fields = split /:/, $lines[-1];
my $uid = $fields[2] + 1;

# Check/set params
if (lc($opt{p}) eq 'ftp') {
        $opt{p} = 104;
} elsif (lc($opt{p}) eq 'sftp') {
        $opt{p} = 2008;
}

$opt{j} = uc($opt{j});
my $home = "/export/home/$opt{u}";

# Backup vfstab
sub bkp_vfstab {
        my $date = strftime '%Y%m%d-%H%M%S', localtime;
        my $vfstab = '/etc/vfstab';
        my $vfstab_bak = "${vfstab}_${date}";
        my $mtime = (stat($vfstab))[9];
        copy ($vfstab, $vfstab_bak) || die "Error: $!\n";
        utime $mtime, $mtime, $vfstab_bak;
        open (FILE, ">>$vfstab") || die "Error: $!\n";
                print FILE "$opt{r}    -       $home       nfs     -       yes bg,noquota\n";
        close FILE;
}

# Create the account
my $useradd = "/usr/sbin/useradd -u $uid -g $opt{p} -d $home -s /bin/false -c $opt{j} $opt{u}";
my $password = substr(Digest::MD5::md5_base64(rand), 0, 10);
system $useradd;
if ($? == 0) {
        mkdir $home, 0777 || die "Error: $!\n";
        chown $uid, $opt{p}, $home || die "Error: $!\n";
        print << "EOF";

Sorry, "passwd --stdin" isn't available on this OS but you can still set a password for this account using the old fashioned way: "passwd $opt{u}" :)

Recommended password is "$password".

Summary:
ID:\t$opt{u}
UID:\t$uid
GID:\t$opt{p}
Home:\t$home

EOF
        bkp_vfstab;
}
